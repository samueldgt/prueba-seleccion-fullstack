import React from 'react'
import { Link, NavLink } from 'react-router-dom'

const Menu = () => {
  const home = {
    pathname: "/",
    state: { reFetch: true }
  }
  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
      <div className="container">
        
        <Link to={home} className="navbar-brand"><img src="/logo.png" height="60" alt="Game of Thrones"/></Link>

        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink className="nav-link" to={home}>Characters</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/about">About</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default Menu