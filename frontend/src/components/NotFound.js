import React from 'react'

const NotFount = () => {
  return (
    <div className="not-found col-sm-12 col-md-8 col-lg-6">
      <h2>404 - Resource not found</h2>
    </div>
  )
}

export default NotFount