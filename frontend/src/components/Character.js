import React from 'react'
import axios from 'axios'
import config from '../env'

class Character extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      character:{},
      loading: true
    }
  }

  getFormattedList = (keyName, list) => {
    return list.map((element,index) => {
      return (
        <li key={keyName + index}>{element}</li>
      )
    })
  }

  getFormattedCharacter = () => {
    let char = this.state.character

    let image = (char.image !== '') ? (<img src={char.image} className="card-img-top" alt={char.name} />) : ''
    let name = (char.name !== '') ? (<h5 className="card-title" key={'card-title'}>{char.name}</h5>) : ''
    let gender = (char.gender !== '') ? (<p key={'l1'}><span className="font-weight-bold">Gender: </span>{char.gender}</p>) : ''
    let slug = (char.slug !== '') ? (<p key={'l2'}><span className="font-weight-bold">Slug: </span>{char.slug}</p>) : ''
    let rank = (char.rank !== '') ? (<p key={'l3'}><span className="font-weight-bold">Rank: </span>{char.rank}</p>) : ''
    let house = (char.house !== '') ? (<p key={'l4'}><span className="font-weight-bold">House: </span>{char.house}</p>) : ''
    let books = (char.books.length !== 0) ? (<><p key={'l5'}><span className="font-weight-bold">Books: </span></p><ul key={'books'}>{this.getFormattedList('book', char.books)}</ul></>) : ''
    let titles = (char.titles.length !== 0) ? (<><p key={'l6'}><span className="font-weight-bold">Titles: </span></p><ul key={'titles'}>{this.getFormattedList('title', char.titles)}</ul></>) : ''

    return(
      <div className="card">
        {image}
        <div className="card-body">
          {name}
          <div className="card-text">
            {gender}
            {slug}
            {rank}
            {house}
            {books}
            {titles}
          </div>
        </div>
      </div>
    )

  }

  fetchData = () => {
    axios.get(`${config.apiURL}/characters/${this.props.match.params.id}`)
    .then((result) => {
      this.setState({
        ...this.state,
        character: result.data,
        loading:false
      })
    })
    .catch((error) => {
      this.setState((prevState, props) => {
        return {
          ...prevState,
          loading: false 
        }
      })
      this.props.setNotification([error.response.data.message] || [error.message, "The app couldn't connect to the server"])
    })
  }
  
  componentDidMount() {
    this.fetchData()
  }
  
  render () {
    return (this.state.loading) ? 
    (
      <div className="start col-sm-12 col-md-6 col-lg-6 text-center mt-5">
        <img src="/loading.gif" className="mt-5" alt="Loading..."/>
      </div>
    ) :
    (
      <div className="character col-sm-12 col-md-8 col-lg-6">
        <h2 className="text-center">Character Details</h2>
        {this.getFormattedCharacter()}
      </div>
    )
  
  }
}

export default Character
