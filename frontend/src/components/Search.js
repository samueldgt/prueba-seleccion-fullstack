import React from 'react'

class Search extends React.Component {

  
  render () {
    return (
      <form onSubmit={this.props.handleSearchSubmit}>
        <div className="form-row justify-content-center">
          <div className="col">
            <input type="text" className="form-control mb-2 text-center" id="inlineFormInput" placeholder="Search by Name or House" onChange={this.props.handleSearchChange} />
          </div>
          <div className="col-auto">
            <button type="submit" className="btn btn-primary mb-2">Send</button>
          </div>
        </div>
      </form>
    )
  }

}

export default Search
