import React from 'react'
import axios from 'axios'
import config from '../env'
import Search from './Search'

class CharactersList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      info: {},
      characters:[],
      loading: true,
      search: ''
    }
  }

  handleSearchSubmit = (e) => {
    e.preventDefault()
    if(this.state.search.length >= 3) {
      this.fetchData(1, this.state.search)
    } else {
      this.props.setNotification(['Your search text must be at least 3 chars long'])
    }
  }

  handleSearchChange = (e) => {
    this.setState({
      ...this.state,
      search: e.target.value
    })
  }

  handleNavigation = (e) => {
    const search = (this.state.info.isSearch) ? this.state.search : ''
    this.setState((prevState, props) => {
      return {
        ...prevState,
        loading: true 
      }
    })
    this.fetchData(e.target.getAttribute('page'), search)
  }

  getFormattedList = (keyName, list, key) => {
    return list.map((element,index) => {
      return (
        <li key={keyName + key + index}>{element}</li>
      )
    })
  }

  getFormattedCharacters = () => {
    return this.state.characters.map((char, index) => {

      let row = ( (index + 1) % 5 === 0 ) ? (<div className="w-100" key={'r' + index}></div>) : ''
      let image = (char.image !== '') ? char.image : '/no-image.png'
      let name = (char.name !== '') ? (<h5 className="card-title" key={'card-title' + index}>{char.name}</h5>) : ''

      return(
        <React.Fragment key={'fragment-'+index}>
          <div className="col" key={index}>
            <div className="card" key={'card' + index}>
              <img src={image} className="card-img-top" alt={char.name || ''} key={'img' + index} />
              <div className="card-body" key={'card-body' + index}>
                {name}
                <button key={'button-link-' + index} className="btn btn-primary" onClick={this.handleGoToProfile} char-id={char._id} >See Details</button>
              </div>
            </div>
          </div>
          {row}
        </React.Fragment>
      )
    })
  }

  handleGoToProfile = (e) => {
    const id = e.target.getAttribute('char-id')
    this.props.history.push(`/character/${id}`)
  }

  getPaginationButtons = () => {
    let first = (typeof this.state.info.prev !== 'undefined' && this.state.info.prev !== null ) ? 
      (
        <li className="page-item" key="first">
          <a className="page-link" href={'#page1'} onClick={this.handleNavigation} page="1" key="f0">First</a>
        </li>
      ) : ''
    let prev = (typeof this.state.info.prev !== 'undefined' && this.state.info.prev !== null ) ? 
      (
        <li className="page-item" key="prev">
          <a className="page-link" href={'#page' + this.state.info.prev} onClick={this.handleNavigation} page={this.state.info.prev} key="f1">Previous</a>
        </li>
      ) : ''
    let next = (typeof this.state.info.next !== 'undefined' && this.state.info.next !== null ) ? 
      (
        <li className="page-item" key="next">
          <a className="page-link" href={'#page' + this.state.info.next} onClick={this.handleNavigation} page={this.state.info.next} key="f2">Next</a>
        </li>
      ) : ''
    let last = (typeof this.state.info.next !== 'undefined' && this.state.info.next !== null ) ? 
      (
        <li className="page-item" key="last">
          <a className="page-link" href={'#page' + this.state.info.pages} onClick={this.handleNavigation} page={this.state.info.pages} key="f3">Last</a>
        </li>
      ) : ''
    return (
      <nav aria-label="Page pagination">
        <ul className="pagination justify-content-center">
          {first}
          {prev}
          {next}
          {last}
        </ul>
      </nav>
    )
  }

  fetchData = (page = 1, search = '') => {
    const searchQuery = (search !== '') ? `&search=${search}` : ''
    axios.get(`${config.apiURL}/characters?page=${page}${searchQuery}`)
    .then((result) => {
      this.setState({
        ...this.state,
        info: {
          prev: result.data.prevPage,
          next: result.data.nextPage,
          pages: result.data.totalPages,
          isSearch: (search !== '')
        }, 
        characters: result.data.characters,
        loading:false
      })
    })
    .catch((error) => {
      this.setState((prevState, props) => {
        return {
          ...prevState,
          loading: false 
        }
      })
      const notification = (typeof error.response !== 'undefined' && typeof error.response.data.message !== 'undefined') ? 
        [error.response.data.message] : [error.message, "The app couldn't connect to the server"]
      this.props.setNotification(notification)
    })
  }
  
  componentDidMount() {
    this.fetchData()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('la puta que lo pario')
    // if ((typeof prevProps.location.state === 'undefined' || typeof prevProps.location.state.reFetch === 'undefined' || !prevProps.location.state.reFetch) && (typeof this.props.location.state !== 'undefined' && this.props.location.state.reFetch)) {
    //   this.fetchData()
    //   this.props.loacation.state.reFetch = false
    // }
  }
  
  render () {
    return (this.state.loading) ? 
    (
      <div className="start col-sm-12 col-md-6 col-lg-6 text-center mt-5">
        <img src="/loading.gif" className="mt-5" alt="Loading..."/>
      </div>
    ) :
    (
      <div className="characters col">
        <h2 className="text-center">Characters</h2>
        <div className="row justify-content-center">
          <div className="search-bar col-sm-12 col-md-8 col-lg-6 ">
            <Search handleSearchChange={this.handleSearchChange} handleSearchSubmit={this.handleSearchSubmit} />
          </div>
        </div>
        <div className="row">
          {this.getFormattedCharacters()}
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-12 col-md-4 col-lg-4 text-center">
            {(this.state.characters.length > 0) ? this.getPaginationButtons() : (<h3>No Characters Found</h3>)}
          </div>
        </div>
      </div>
    )
  
  }
}

export default CharactersList
