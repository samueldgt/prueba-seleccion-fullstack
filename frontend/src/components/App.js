import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Menu from './Menu'
import CharactersList from './CharactersList'
import Character from './Character'
import About from './About'
import NotFound from './NotFound'

class App extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      errors: []
    }
  }

  setNotification = (errors = []) => {
    this.setState({
      ...this.state,
      errors
    })
  }

  getFormattedNotifications = () => {
    if (this.state.errors.length > 0) {
      return this.state.errors.map( (error, key) => {
        return (
          <div key={key} className="col-12 text-center alert alert-danger" role="alert">
            { error }
          </div>
        )
      })
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if (prevState.errors.length === 0 && this.state.errors.length > 0) {
      setTimeout(() => this.setState({
        ...this.state, 
        errors: []
      }), 3000);
    }
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Menu />
          <div className="container content">
            <div className="row justify-content-center">
              {this.getFormattedNotifications()}
              <Switch>
                <Route exact path="/" render={ (props) => <CharactersList {...props} setNotification={this.setNotification} /> } />
                <Route exact path="/character/:id" render={ (props) => <Character {...props} setNotification={this.setNotification} /> } />

                <Route path="/about" component={About} />
                <Route path="*" component={NotFound} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    )
  }

}

export default App
