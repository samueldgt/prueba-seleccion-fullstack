const axios = require('axios')
const CharacterModel = require('../models/Character')

class CharactersLoader {

  constructor () {
    this.errors = []
  }
  
  isEmptyDatabase = async () => {
    let count = 0
    try {
      count = await CharacterModel.estimatedDocumentCount()
    } catch (error) {
      this.errors.push('Error when counting documents in Database')
      console.error(error)
    }
    return (count == 0)
  }
  
  getCharacters = async (url) => {
    let response = []
    try {
      response = await axios.get(url)
    } catch (error) {
      this.errors.push('Error getting the characters from the GOT third-party Api')
      // error.response.data
      console.error(error)
    }
    return response
  }

  mapCharacters = (charsFromShow, charsFromBooks) => {
    let booksByChar = {}

    charsFromBooks.forEach(character => {
      booksByChar[character.slug] = character.books || []
    })
    
    const mappedCharacters = charsFromShow.map(character => {
      return {
        name: character.name || '',
        image: character.image || '',
        gender: character.gender || '',
        slug: character.slug || '',
        rank: (typeof character.pagerank !== 'undefined' && typeof character.pagerank.rank !== 'undefined') ? character.pagerank.rank : '',
        house: character.house || '',
        books: (typeof booksByChar[character.slug] !== 'undefined') ? booksByChar[character.slug] : [] ,
        titles: character.titles || []
      }
    })

    return mappedCharacters
  }

  saveCharacters = async (mappedCharacters) => {
    try {
      await CharacterModel.create(mappedCharacters)
    } catch (error) {
      this.errors.push('Error inserting the characters in database')
      console.error(error)
    }
  }

  load = async () => {
    if (await this.isEmptyDatabase()) {
      const response = await this.getCharacters(process.env.GOT_API_URL)
      const mappedCharacters = this.mapCharacters(response.data.show, response.data.book)
      await this.saveCharacters(mappedCharacters)
    }
  }

  removeAll = async () => {

    try {
      await CharacterModel.deleteMany({})
    } catch (error) {
      this.errors.push('Error when deleting all the documents in Database')
      console.error(error)
    }
  } 

}

module.exports = CharactersLoader