const characterModel = require('../models/Character')
const escapeReguex = require('../utils/escapeReguex')

const customLabels = {
  docs: 'characters',
  totalDocs: 'totalCharacters',
  hasNextPage: false,
  hasPrevPage: false,
  pagingCounter: false,
  meta: false
}

let options = {
  limit: 10,
  sort: { createdOn: 1 },
  customLabels
}

let query = {}

exports.index = async (req, res) => {
  try {
    options.page = parseInt(req.query.page) || 1
    
    if (typeof req.query.search !== 'undefined' && req.query.search !== '') {
      const search = escapeReguex(req.query.search)
      query = { 
        '$or': [
          {name: { $regex: search, $options: 'i' }},
          {house: { $regex: search, $options: 'i' }}
        ]
      }
    }
    const characters = await characterModel.paginate(query, options)
    res.status(200).json(characters)
  } catch (error) {
    console.log(error)
    res.status(500).json({message: error.message})
  }  
}

exports.show = async (req, res) => {
  try {
    const id = req.params.id
    const character = await characterModel.findById(id)
    res.status(200).json(character)
  } catch (error) {
    console.log(error)
    res.status(500).json({message: error.message})
  }
}