require('dotenv').config()
const express = require('express')
const helmet = require('helmet') // Increases API security by headers
const cors = require('cors') // Adds CORS (accepts requests coming from other origins)
const port = process.env.BACKEND_SERVER_PORT
const DBConnector = require('./utils/DBConnector')
const CharactersLoader = require('./services/CharactersLoader')
const ErrorHandler = require('./utils/ErrorHandler')
const charactersRouter = require('./routes/charactersRouter')

const db = new DBConnector()
const app = express()
const handler = new ErrorHandler()

// third-party configuration 
app.use(helmet())
app.use(cors())
app.use(express.json())


if (db.connect()) {
  const loader = new CharactersLoader()
  loader.load()

  //Routing area

  app.use('/characters', charactersRouter)
  
  app.use(handler.notFound)
  app.use(handler.serverError)
} else {
  app.use(handler.dbError)
}

app.listen(port, () => {
  console.info('[backend]',`Server is running on port ${port}`)
})