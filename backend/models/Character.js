const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const characterSchema = new mongoose.Schema({
  name: String,
  image: String,
  gender: String,
  slug: String,
  rank: Number,
  house: String,
  books: [],
  titles: [],
  createdOn: {
    type: Date,
    default: Date.now
  }
})

characterSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Characters', characterSchema)