const mongoose = require('mongoose')

class DBConnector {
  constructor() {
    this.databaseUrl = `${process.env.DATABASE_HOST}${process.env.DATABASE_NAME}` 
    this.mongooseConfig = {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
    this.isConnected = false
  }
  
  connect = async () => {
    try {
      await mongoose.connect(this.databaseUrl, this.mongooseConfig)
      this.isConnected = true
      console.info('[backend]','Database connected successfully')
    }catch (error) {
      console.error('DB connection has failed')
      console.error(error.message)
      console.error(error.reason)
      this.isConnected = false
    }
    return this.isConnected
  }
}

module.exports = DBConnector