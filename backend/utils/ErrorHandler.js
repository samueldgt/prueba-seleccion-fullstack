class ErrorHandler {

  notFound = (req, res, next) => {
    let err = new Error('Resource not found')
    err.status = 404
    next(err)
  }

  serverError = (err, req, res, next) => {
    res.status(500).json({
      message: err.message || 'Internal server error'
    })
  }

  dbError = (req, res, next) => {
    res.status(500).json({
      message: 'DB connection has failed'
    })
  }
}

module.exports = ErrorHandler 